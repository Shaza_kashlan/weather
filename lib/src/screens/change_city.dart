import "dart:math";


import 'package:WeatherForecast/main.dart';
import 'package:WeatherForecast/src/model/city.dart';
import 'package:flutter/material.dart';

import 'dart:math';

import 'package:WeatherForecast/src/model/city.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:WeatherForecast/src/screens/routes.dart';
import 'package:WeatherForecast/src/screens/weather_screen.dart';
import 'package:bloc/bloc.dart';
import 'package:WeatherForecast/src/themes.dart';
import 'package:WeatherForecast/src/utils/constants.dart';
import 'package:WeatherForecast/src/utils/converters.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ChangeCityScreen extends StatefulWidget {
  ChangeCityScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ChangeCityScreenState createState() => _ChangeCityScreenState();
}

class _ChangeCityScreenState extends State<ChangeCityScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text("Cities"),
            automaticallyImplyLeading: true,
            //`true` if you want Flutter to automatically add Back Button when needed,
            //or `false` if you want to force your own back button every where
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              //onPressed:() => Navigator.pop(context, false),
              onPressed: () => Navigator.pop(context, radioItemHolder),
            )),
        backgroundColor: Colors.white,
        body: Material(
          child: Container(
            constraints: BoxConstraints.expand(),
            child: RadioGroup(),
          ),
        ));
  }
}

String radioItemHolder = 'Kuala Lumpur';

class RadioGroup extends StatefulWidget {
  @override
  RadioGroupWidget createState() => RadioGroupWidget();
}


class RadioGroupWidget extends State<RadioGroup> {
  var cityList = List<City>();
  // WeatherBloc _weatherBloc;
  @override
  void initState() {
    super.initState();

  }


  // Group Value for Radio Button.
  int id = 0;
  // generates a new Random object


  Widget build(BuildContext context) {
    this.cityList = AppStateContainer.of(context).cities;
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(14.0),
          //  child: Text('Selected Item = '+'$radioItemHolder', style: TextStyle(fontSize: 23)),
        ),
        Expanded(
            child: Container(
          height: 350.0,
          child: Column(
            children: cityList
                .map((data) => RadioListTile(
                      title: Text("${data.city}"),
                      groupValue: id,
                      value: data.index,
                      onChanged: (val) {
                        setState(() {
                          radioItemHolder = data.city;
                          id = data.index;
                         Navigator.pop(context, radioItemHolder);
                        });
                      },
                    ))
                .toList(),
          ),
        )),
      ],
    );
    //    }
    //  },
    //  );
  }
}
