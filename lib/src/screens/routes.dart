import 'package:flutter/material.dart';
import 'package:WeatherForecast/src/screens/settings_screen.dart';
import 'package:WeatherForecast/src/screens/weather_screen.dart';

import 'change_city.dart';

class Routes {
  static final mainRoute = <String, WidgetBuilder>{
    '/home': (context) => WeatherScreen(),
    '/settings': (context) => SettingsScreen(),
  };
}
