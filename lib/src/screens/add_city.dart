import 'dart:convert';

import 'package:WeatherForecast/src/model/city.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Exercise {
  String name;
  Exercise({this.name});
}

class AddCity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: AddCityScreen(title: 'Flutter Demo Home Page'),
    );
  }
}

class AddCityScreen extends StatefulWidget {
  AddCityScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AddCityScreenState createState() => _AddCityScreenState();
}
Future<String> loadAsset() async {
  return await rootBundle.loadString('assets/Cities.json');
}
class _AddCityScreenState extends State<AddCityScreen> {
  int _selected;
  String _selectedValue;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Container(
          child: Center(
            // Use future builder and DefaultAssetBundle to load the local JSON file
            child: FutureBuilder(
                future: loadAsset(),
                builder: (context, data) {
                  List<City> citiesData = List<City>();
                  if(data.data != null){
                    citiesData = City.decodeCities(data.data.toString());
                  }
                  return AlertDialog(
                    title: Text("Cities"),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    actions: <Widget>[
                      FlatButton(
                        child: const Text('CANCEL'),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        textColor: Theme.of(context).accentColor,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                        child: const Text('OK'),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        textColor: Theme.of(context).accentColor,
                        onPressed: () {
                          //widget.onOk();
                          Navigator.pop(context, _selectedValue);
                        },
                      ),
                    ],
                    content: SingleChildScrollView(
                      child: Container(
                        width: double.maxFinite,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Divider(),
                            ConstrainedBox(
                              constraints: BoxConstraints(
                                maxHeight:
                                    MediaQuery.of(context).size.height * 0.4,
                              ),
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: citiesData.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return RadioListTile(
                                        title: Text(citiesData[index].city),
                                        value: index,
                                        groupValue: _selected,
                                        onChanged: (value) {
                                          setState(() {
                                            _selected = index;
                                            _selectedValue =
                                            citiesData[index].city;
                                          });
                                        });
                                  }),
                            ),
                            Divider(),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ),
        )
    );

  }
}

