import 'package:WeatherForecast/src/resourses/weather_api_provider.dart';
import 'package:WeatherForecast/src/model/weather.dart';
import 'package:meta/meta.dart';

class WeatherRepository {
  final WeatherApiProvider weatherApiProvider;
  WeatherRepository({@required this.weatherApiProvider})
      : assert(weatherApiProvider != null);

  Future<Weather> getWeather(String cityName,
      {double latitude, double longitude}) async {
    if (cityName == null) {
      cityName = await weatherApiProvider.getCityNameFromGeographic(
          latitude: latitude, longitude: longitude);

    }

    var weather = await weatherApiProvider.getWeatherFromCityName(cityName);
    var weathers = await weatherApiProvider.getWeatherForecastFromCityName(cityName);
    weather.forecast = weathers;
    return weather;
  }
}
