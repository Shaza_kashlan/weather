import 'dart:convert';

class City {
  String city;
  int index;

  City({this.city, this.index});

  factory City.fromJson(Map<String, dynamic> jsonData) {
    return City(
      index: jsonData['index'],
      city: jsonData['city'],

    );
  }

  static Map<String, dynamic> toMap(City city) => {
    'index': city.index,
    'city': city.city,
  };

  static String encodeCities(List<City> musics) => json.encode(
    musics
        .map<Map<String, dynamic>>((city) => City.toMap(city))
        .toList(),
  );

  static List<City> decodeCities(String cities) =>
      (json.decode(cities) as List<dynamic>)
          .map<City>((item) => City.fromJson(item))
          .toList();
}