import 'package:WeatherForecast/src/utils/weather_icon_Mapper.dart';
import 'package:flutter/material.dart';
import 'package:WeatherForecast/src/utils/converters.dart';

class Weather {
  int id;
  int date;
  int sunrise;
  int sunset;
  int humidity;

  double windSpeed;

  String cityName;
  String main;
  String description;
  String iconCode;

  Temperature temperature;
  Temperature maxTemperature;
  Temperature minTemperature;

  List<Weather> forecast;


  Weather ({this.id,this.date,this.sunrise,this.sunset, this.humidity,this.windSpeed,
           this.cityName,this.main,this.description,this.iconCode,
           this.temperature,this.maxTemperature,this.minTemperature,this.forecast});


 static Weather mapJson (Map<String,dynamic> weatherJson){
    final weather = weatherJson['weather'][0];
    return Weather(
      id : weather['id'],
      date: weatherJson['dt'],
      sunrise: weatherJson['sys']['sunrise'],
      sunset: weatherJson['sys']['sunset'],
      humidity: weatherJson['main']['humidity'],
      windSpeed: intToDouble(weatherJson['wind']['speed']),
      cityName: weatherJson['name'],
      main: weather['main'],
      description: weather['description'],
      iconCode: weatherJson['icon'],
      temperature: Temperature(intToDouble (weatherJson['main']['temp'])),
      maxTemperature: Temperature(intToDouble(weatherJson['main']['temp_max'])),
      minTemperature: Temperature(intToDouble(weatherJson['main']['temp_min']))
    );

 }

 static List<Weather> mapForecastJson(Map<String,dynamic> forecastJson){
     final forecastList = List<Weather>();
     for (final item in forecastJson['list']) {
       forecastList.add(Weather(
           date: item['dt'],
           temperature: Temperature(intToDouble(
             item['main']['temp'],
           )),
           iconCode: item['weather'][0]['icon']
       ));
     }
     return forecastList;
  }

  IconData getIconData(){
    switch(this.iconCode){
      case '01d': return WeatherIcons.clear_day;
      case '01n': return WeatherIcons.clear_night;
      case '02d': return WeatherIcons.few_clouds_day;
      case '02n': return WeatherIcons.few_clouds_day;
      case '03d':
      case '04d':
        return WeatherIcons.clouds_day;
      case '03n':
      case '04n':
        return WeatherIcons.clear_night;
      case '09d': return WeatherIcons.shower_rain_day;
      case '09n': return WeatherIcons.shower_rain_night;
      case '10d': return WeatherIcons.rain_day;
      case '10n': return WeatherIcons.rain_night;
      case '11d': return WeatherIcons.thunder_storm_day;
      case '11n': return WeatherIcons.thunder_storm_night;
      case '13d': return WeatherIcons.snow_day;
      case '13n': return WeatherIcons.snow_night;
      case '50d': return WeatherIcons.mist_day;
      case '50n': return WeatherIcons.mist_night;
      default: return WeatherIcons.clear_day;
    }
  }



}