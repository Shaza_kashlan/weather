import 'dart:convert';
import 'dart:math';

import 'package:WeatherForecast/src/model/city.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:WeatherForecast/src/screens/routes.dart';
import 'package:WeatherForecast/src/screens/weather_screen.dart';
import 'package:bloc/bloc.dart';
import 'package:WeatherForecast/src/themes.dart';
import 'package:WeatherForecast/src/utils/constants.dart';
import 'package:WeatherForecast/src/utils/converters.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() {
  BlocSupervisor().delegate = SimpleBlocDelegate();
  runApp(AppStateContainer(child: WeatherApp()));
}




class SimpleBlocDelegate extends BlocDelegate {
  @override
  onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}


class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather App',
      theme: AppStateContainer.of(context).theme,
      home: WeatherScreen(),
      routes: Routes.mainRoute,
    );
  }
}


class AppStateContainer extends StatefulWidget {
  final Widget child;

  AppStateContainer({@required this.child});

  @override
  _AppStateContainerState createState() => _AppStateContainerState();

  static _AppStateContainerState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_InheritedStateContainer)
    as _InheritedStateContainer)
        .data;
  }
}
//class City {
//  String city;
//  int index;
//
//  City({this.city, this.index});
//
//  factory City.fromJson(Map<String, dynamic> jsonData) {
//    return City(
//      index: jsonData['index'],
//      city: jsonData['city'],
//
//    );
//  }
//
//  static Map<String, dynamic> toMap(City city) => {
//    'index': city.index,
//    'city': city.city,
//  };
//
//  static String encodeCities(List<City> musics) => json.encode(
//    musics
//        .map<Map<String, dynamic>>((city) => City.toMap(city))
//        .toList(),
//  );
//
//  static List<City> decodeCities(String cities) =>
//      (json.decode(cities) as List<dynamic>)
//          .map<City>((item) => City.fromJson(item))
//          .toList();
//}

class _AppStateContainerState extends State<AppStateContainer> {
  ThemeData _theme = Themes.getTheme(Themes.LIGHT_THEME_CODE);
  int themeCode = Themes.LIGHT_THEME_CODE;
  TemperatureUnit temperatureUnit = TemperatureUnit.celsius;
  var randomGenerator;
  var index;
  List<String> nList = ["Kuala Lumpur","George Town","Johor Bahru"];
  List<City> cities = List<City>() ;


  @override
  initState() {
    super.initState();
    for (var i=0; i<nList.length; i++) {
     // this.randomGenerator = Random();
     // this.index = randomGenerator.nextInt(nList.length+i);
      this.cities.add(City(index: i,city: nList[i]));

    }
    final String encodedData = City.encodeCities(cities);
    SharedPreferences.getInstance().then((sharedPref) {

      sharedPref.setString(CONSTANTS.SHARED_PREF_KEY_ADDED_CITY, encodedData);
    });

    SharedPreferences.getInstance().then((sharedPref) {
      setState(() {
        themeCode = sharedPref.getInt(CONSTANTS.SHARED_PREF_KEY_THEME) ??
            Themes.LIGHT_THEME_CODE;
        temperatureUnit = TemperatureUnit.values[
        sharedPref.getInt(CONSTANTS.SHARED_PREF_KEY_TEMPERATURE_UNIT) ??
            TemperatureUnit.celsius.index];
        this._theme = Themes.getTheme(themeCode);

        this.cities = City.decodeCities(sharedPref.getString(CONSTANTS.SHARED_PREF_KEY_ADDED_CITY));

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    print(theme.accentColor);
    return _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }

  ThemeData get theme => _theme;

  updateTheme(int themeCode) {
    setState(() {
      _theme = Themes.getTheme(themeCode);
      this.themeCode = themeCode;
    });
    SharedPreferences.getInstance().then((sharedPref) {
      sharedPref.setInt(CONSTANTS.SHARED_PREF_KEY_THEME, themeCode);
    });
  }

  updateTemperatureUnit(TemperatureUnit unit) {
    setState(() {
      this.temperatureUnit = unit;
    });
    SharedPreferences.getInstance().then((sharedPref) {
      sharedPref.setInt(CONSTANTS.SHARED_PREF_KEY_TEMPERATURE_UNIT, unit.index);
    });
  }

  updateAddedList(String cityName) {
    setState(() {

     this.cities.add(City(index: cities.length+1,city: cityName));
    });
    final String encodedData = City.encodeCities(cities);
     SharedPreferences.getInstance().then((sharedPref) {

      sharedPref.setString(CONSTANTS.SHARED_PREF_KEY_ADDED_CITY, encodedData);
    });
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final _AppStateContainerState data;

  const _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer oldWidget) => true;
}
