#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "AccessSettingsMenuPlugin.h"

FOUNDATION_EXPORT double access_settings_menuVersionNumber;
FOUNDATION_EXPORT const unsigned char access_settings_menuVersionString[];

